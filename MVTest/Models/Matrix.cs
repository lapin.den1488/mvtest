﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVTest.Models
{
    public class Matrix
    {
        public int Rows { get; set; }
        public int Columns { get; set; }
        public List<int []> Values { get; set; }

        
        public Matrix()
        {
            
        }

        public Matrix(int Rows,int Columns)
        {
            this.Rows = Rows;
            this.Columns = Columns;
            this.Values = new List<int[]>();

            for(int i = 0; i < Columns; i++)
            {
                int[] zero_arr = new int[Rows];
                for(int j = 0; j < Rows; j++)
                {
                    zero_arr[j] = 0;
                }
                this.Values.Add(zero_arr);
            }
        }
    }
}