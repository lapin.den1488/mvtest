﻿using MVTest.Helpers;
using System.Web;

namespace MVTest.Models.ViewModel
{
    public class MatrixViewModel
    {
        [FileValidator(ErrorMessage = "Please upload the .csv file")]
        public HttpPostedFileBase FileObj { get; set; }

        public Matrix MatrixView { get; set; }

        public MatrixViewModel()
        {
            this.MatrixView = new Matrix(5, 5);
        }
    }
}