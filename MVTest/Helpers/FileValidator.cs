﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;

namespace MVTest.Helpers
{
    public class FileValidator : RequiredAttribute
    {
        public override bool IsValid(object value)
        {
            
            try
            {
                var file = value as HttpPostedFileBase;
                if (file == null)
                {
                    return false;
                }
                if(Path.GetExtension(file.FileName) != ".csv")
                {
                    return false;
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}