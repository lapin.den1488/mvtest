﻿function confirmRedirect(link) {
    $("#dialog-confirm").dialog({
        hide: { effect: "fade", duration: 1000 },
        show: { effect: "fade", duration: 800 },
        resizable: false,
        height: "auto",
        width: 400,
        modal: true,
        buttons: {            
            "Delete": function () {
                $(this).dialog("close");
                location.href = link;
            },
            Cancel: function () {
                $(this).dialog("close");
            }
        }
    });
}